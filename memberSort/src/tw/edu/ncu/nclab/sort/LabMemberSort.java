package tw.edu.ncu.nclab.sort;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javax.swing.JOptionPane;

public class LabMemberSort {

	private ArrayList<String> memberList = new ArrayList<String>(0);
	private int numberOfRuns = 0;

	public static void main(String[] args) {
		new LabMemberSort();
	}

	public LabMemberSort() {
		initial();
		showInput();
		startShuffle();
		showResults();
	}

	private void initial() {
		try (BufferedReader br = new BufferedReader(new FileReader(new File(
				"LabMember.txt")));) {

			while (true) {
				String name = br.readLine();
				if (name == null) {
					break;
				} else if (name.equals("")) {
					continue;
				} else {
					memberList.add(name);
				}
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	private void showInput() {
		StringBuffer sb = new StringBuffer();
		sb.append("NCLab\n");

		sb.append("共有" + memberList.size() + "名成員" + memberList + "\n");
		sb.append("請輸入排序的次數(正整數)");

		do {
			String input = JOptionPane.showInputDialog(sb.toString(), null);
			try {
				numberOfRuns = Integer.parseInt(input);
			} catch (NumberFormatException e) {
				numberOfRuns = -1;
			}
		} while (numberOfRuns <= 0);
	}

	private void startShuffle() {
		for (int i = 1; i <= numberOfRuns; i++) {
			Collections.shuffle(memberList,new Random(System.currentTimeMillis()));

		}
	}

	private void showResults() {
		String outcome = "順序為：" + memberList;
		JOptionPane.showMessageDialog(null, outcome);
		System.out.println(outcome);
	}

}
